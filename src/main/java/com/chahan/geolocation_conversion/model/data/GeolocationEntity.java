package com.chahan.geolocation_conversion.model.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "geolocations")
public class GeolocationEntity {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Long id;
  private String searchHash;
  private String address;
  private String lon;
  private String lat;
}
