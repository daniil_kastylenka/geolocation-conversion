package com.chahan.geolocation_conversion.mapper;

import com.chahan.geolocation_conversion.client.model.BingMapsApiResponse;
import com.chahan.geolocation_conversion.model.data.GeolocationEntity;
import com.chahan.geolocation_conversion.model.dto.Geolocation;
import com.chahan.geolocation_conversion.model.dto.GeolocationResponse;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-10-11T22:24:59+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.8-internal (N/A)"
)
@Component
public class GeolocationMapperImpl extends GeolocationMapper {

    @Override
    public GeolocationResponse map(GeolocationEntity source, String search) {
        if ( source == null && search == null ) {
            return null;
        }

        GeolocationResponse geolocationResponse = new GeolocationResponse();

        if ( source != null ) {
            geolocationResponse.setAddress( source.getAddress() );
            geolocationResponse.setLon( source.getLon() );
            geolocationResponse.setLat( source.getLat() );
        }
        if ( search != null ) {
            geolocationResponse.setSearch( search );
        }

        return geolocationResponse;
    }

    @Override
    public Geolocation map(BingMapsApiResponse source, String search) {
        if ( source == null && search == null ) {
            return null;
        }

        Geolocation geolocation = new Geolocation();

        if ( search != null ) {
            geolocation.setSearch( search );
        }
        geolocation.setAddress( getAddress(source) );
        geolocation.setLon( getLon(source) );
        geolocation.setLat( getLat(source) );

        return geolocation;
    }

    @Override
    public GeolocationEntity map(Geolocation source, String searchHash, String search) {
        if ( source == null && searchHash == null && search == null ) {
            return null;
        }

        GeolocationEntity geolocationEntity = new GeolocationEntity();

        if ( source != null ) {
            geolocationEntity.setAddress( source.getAddress() );
            geolocationEntity.setLon( source.getLon() );
            geolocationEntity.setLat( source.getLat() );
        }
        if ( searchHash != null ) {
            geolocationEntity.setSearchHash( searchHash );
        }

        return geolocationEntity;
    }

    @Override
    public GeolocationEntity mapWithCoordinates(Geolocation source, String lon, String lat) {
        if ( source == null && lon == null && lat == null ) {
            return null;
        }

        GeolocationEntity geolocationEntity = new GeolocationEntity();

        if ( source != null ) {
            geolocationEntity.setAddress( source.getAddress() );
        }
        if ( lon != null ) {
            geolocationEntity.setLon( lon );
        }
        if ( lat != null ) {
            geolocationEntity.setLat( lat );
        }

        return geolocationEntity;
    }
}
